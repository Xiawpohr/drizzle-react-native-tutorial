# Dapp with React Native
## Folder Structur
### Truffle
Truffle requires an empty folder to start, so let's initialize it first before our React Native project:

```
mkdir truffle-temp
cd truffle-temp
truffle init
```

You should have the following inside the project folder:
├── contracts
├── migrations
├── test
└── truffle-config.js

### React Native
1. Initialize your React Native project in its own folder, as a sibling folder of your Truffle project ==truffle-temp==:
```
react-native init DrizzleReactNativeTutorial
```
2. React Native and Truffle folders should be in root since React Native doesn't allow you to use symlinks yet, and cannot import from files outside the React Native project folder.

Copy all the files in ==truffle-temp== into the root folder of your React Native project. Your folder should look like this in the end:

├── android
├── contracts
├── ios
├── migrations
├── node_modules
├── test
├── App.js
├── app.json
├── index.js
├── package.json
├── truffle-config.js
├── truffle.js
└── yarn.lock

## Setup
React Native is missing some of the global objects that are available on other platforms such as the web or Node. We will have to provide our own (i.e. a shim) through imported libraries or in some cases our own code.

1. Install node-libs-react-native, vm-browserify, Base64, and react-native-randombytes:
```
yarn add node-libs-react-native vm-browserify Base64 react-native-randombytes
```

2. Link the native libraries in react-native-randombytes:
```
react-native link react-native-randombytes
```

3. Create a new file shims.js in the root folder with the following code:
```js
import "node-libs-react-native/globals";
import { btoa } from "Base64";
import nodeUrl from 'url';

global.btoa = btoa;
global.URL = class URL {
    constructor(url) {
        return nodeUrl.parse(url)
    }
}

/**
 * From https://github.com/facebook/react-native/blob/1151c096dab17e5d9a6ac05b61aacecd4305f3db/Libraries/polyfills/Object.es6.js
 * This on RN's master branch as of Sep 11, 2018, however it has not made it into a release yet.
 *
 * The only modification made in Truffle's polyfill was to remove the check for an existing implementation.
 * RN 0.57.7 (and below I assume) uses the non-spec compliant Object.assign that breaks in dev RN builds
 * https://github.com/facebook/react-native/issues/16814
 */
Object.defineProperty(Object, 'assign', {
  value: function assign(target, varArgs) {
    'use strict';
    if (target == null) {
      throw new TypeError('Cannot convert undefined or null to object');
    }

    let to = Object(target);

    for (let index = 1; index < arguments.length; index++) {
      let nextSource = arguments[index];

      if (nextSource != null) {
        for (let nextKey in nextSource) {
          if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
    return to;
  },
  writable: true,
  configurable: true,
}); 
```

4. Create a new file rn-cli.config.js in the root folder with the following code:

```js
const nodeLibs = require("node-libs-react-native");
nodeLibs.vm = require.resolve("vm-browserify");

module.exports = {
  resolver: {
    extraNodeModules: nodeLibs
  },
  serializer: {
    // From https://github.com/facebook/react-native/blob/v0.57.7/rn-get-polyfills.js
    getPolyfills: () => [
      /**
     * We omit RN's Object.assign polyfill
     * If we don't, then node_modules will be using RN's polyfill rather than ours.
     */
      // require.resolve('react-native/Libraries/polyfills/Object.es6.js'),
      require.resolve('react-native/Libraries/polyfills/console.js'),
      require.resolve('react-native/Libraries/polyfills/error-guard.js'),
      require.resolve('react-native/Libraries/polyfills/Number.es6.js'),
      require.resolve('react-native/Libraries/polyfills/String.prototype.es6.js'),
      require.resolve('react-native/Libraries/polyfills/Array.prototype.es6.js'),
      require.resolve('react-native/Libraries/polyfills/Array.es6.js'),
      require.resolve('react-native/Libraries/polyfills/Object.es7.js'),
    ]
  }
};
```

5. Install rn-nodeify
```
npm install --save rn-nodeify
./node_modules/.bin/rn-nodeify --install --hack
```

6. Finally let's import our shims in index.js. The very first line should be the following:

```js
import "./shims"
import "./shim
```

## Start the project
### Connecting your app to your Ganache testnet
When we're Working with React Native and mobile apps, accessing the Ganache server that's running on your machine takes a bit more work than when we are building web apps. The sections below detail how to connect to the Ganache testnet with your mobile device/emulator.

#### Running the app
1. Start React Native Metro bundler: react-native start
2. Start your emulator/plug in your device

#### Android (Emulator/Physical Device)
The main thing for Android devices is that we have to reverse the ports so that we can point to localhost on the Android device to the Ganache server.

Make sure you've setup the Android Debug Bridge (adb) before doing these steps.

1. Start ganache-cli: `ganache-cli -b 3`
2. Compile and migrate contracts: `truffle compile && truffle migrate`
3. Reverse ports: `adb reverse tcp:8545 tcp:8545`
4. Install app: `react-native run-android`

#### iOS
##### Simulator
The iOS simulator will see servers on localhost just fine.

1. Start ganache-cli: `ganache-cli -b 3`
2. Compile and migrate contracts: `truffle compile && truffle migrate`
3. Install app: `react-native run-ios` (you can also do this through Xcode)

##### Physical device
iOS physical devices involve the most manual work relative to other devices. You have to look up the local IP address of your machine and manually handle it every time it changes.

1. Find your LOCAL_MACHINE_IP by checking your network settings on your Mac where Ganache is running
2. Start ganache-cli: `ganache-cli -b 3 -h LOCAL_MACHINE_IP`
In truffle.js for development, point Truffle to LOCAL_MACHINE_IP
3. Compile and migrate contracts: `truffle compile && truffle migrate`
4. In index.js, point Drizzle to LOCAL_MACHINE_IP
```js
const options = {
  ...
  web3: {
    fallback: {
      type: "ws",
      url: "ws://LOCAL_MACHINE_IP:8545"
    }
  }
};
```
6. Install: Do it through Xcode
