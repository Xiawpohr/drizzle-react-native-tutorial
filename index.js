/**
 * @format
 */

import './shim.js'
import './globals-shim.js'
import React from 'react'
import { AppRegistry } from 'react-native'
import { Drizzle, generateStore } from 'drizzle'
import App from './app/App'
import { name as appName } from './app.json'
import MyStringStore from './build/contracts/MyStringStore.json'

const options = {
  contracts: [MyStringStore]
}
const drizzleStore = generateStore(options)
const drizzle = new Drizzle(options, drizzleStore)

AppRegistry.registerComponent(appName, () => () => <App drizzle={drizzle} />)
