/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View } from 'react-native'
import getWeb3 from '@drizzle-utils/get-web3'
import createDrizzleUtils from '@drizzle-utils/core'
import ReadString from './ReadString.js'
import SetString from './SetString'

type Props = {}
export default class App extends Component<Props> {
  state = {
    loading: true,
    drizzleState: null,
    web3Load: false,
    web3: null
  }

  loadWeb3 = async () => {
    const web3 = await getWeb3()
    if (web3) this.setState({ web3Load: true, web3 })
  }

  componentDidMount() {
    this.loadWeb3()
    const { drizzle } = this.props

    this.unsubscribe = drizzle.store.subscribe(() => {
      const drizzleState = drizzle.store.getState()

      if (drizzleState.drizzleStatus.initialized) {
        this.setState({ loading: false, drizzleState })
      }
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {
    const web3 = this.state.web3
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <Text>Loading...</Text>
        ) : (
          <View>
            <Text>{web3 ? web3.eth.defaultBlock : 'no web3'}</Text>
            <ReadString
              drizzle={this.props.drizzle}
              drizzleState={this.state.drizzleState}
            />
            <SetString
              drizzle={this.props.drizzle}
              drizzleState={this.state.drizzleState}
            />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
})